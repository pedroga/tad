export module Tad.Rpgs:Common;

import std;
import Tad.Utils;
import Tad.Io;
import Tad.Common;

using namespace std;
using namespace Utils;


export namespace Rpgs {

// Skills.
//---------------------------------------------------------------------------------------------------------------------
struct Skill_level {
   int level{};
   int bonus{};
   int req{};
   char desc[32]{ "" };
};

vector<Skill_level> skill_levels;

Skill_level get_skill_level(int level)
{
   if (level >= skill_levels.back().level) return skill_levels.back();
   for (auto &s : ranges::reverse_view{ skill_levels })
      if (s.level <= level) return s;
   return skill_levels.front();
}

// Wounds.
//---------------------------------------------------------------------------------------------------------------------
struct Wound_level {
   int level{};
   int required_damage{};
   char desc[32]{ "" };
};

vector<Wound_level> wound_levels;

using Wounds_table = vector<int>;

Wound_level damage_level(int damage)
{
   for (const Wound_level &wl : ranges::reverse_view{ wound_levels })
      if (damage >= wl.required_damage) return wl;
   return wound_levels[0];
}

// Health.
//---------------------------------------------------------------------------------------------------------------------
struct Health_level {
   int level{};
   char desc[32]{ "" };
};

vector<Health_level> health_levels;

Health_level health_level(const Wounds_table &wounds, int toughness)
{
   Wounds_table wounds_cpy{ wounds }; // Copy so we don't modify the original.
   bool can_be_killed = false;
   for (Wound_level &wl : wound_levels) {
      if (wl.level <= 0) continue;
      if (wl.level >= 2 && wounds[wl.level]) can_be_killed = true;
      while (wounds_cpy[wl.level] > max((1 << toughness) >> wl.level, 1)) {// Wounds beyond capacity move to next level.
         --wounds_cpy[wl.level];
         ++wounds_cpy[wl.level + 1];
      }
   }
   for (const Wound_level &wl : ranges::reverse_view{ wound_levels }) {
      if (wounds_cpy[wl.level]) {
         if (wl.level == 4 && !can_be_killed) return health_levels[wl.level];
         return health_levels[wl.level + 1];
      }
   }
   return health_levels[0];
}
// Fight logs.
//---------------------------------------------------------------------------------------------------------------------
array<vector<const char *>, 4> fight_logs;


// Init game data.
//---------------------------------------------------------------------------------------------------------------------
void init_data(Common::Language lang_idx)
{
   Io::read_csv_data("../Assets/Data/skill_levels.csv", [lang_idx](const char *line) {
      constexpr int skill_idx{ 0 }, bonus_idx{ 1 }, req_idx{ 2 }, desc_idx{ 3 };
      static int current_line{ 0 };
      stringstream ss{ line };
      char word[256]{ "" };
      int current_idx{ 0 };
      Skill_level skill{};
      while (ss.getline(word, 1024, ',')) {
         if (current_line == 0) continue; // Header line.
         if (current_idx == skill_idx) skill.level = atoi(word);
         else if (current_idx == bonus_idx) skill.bonus = atoi(word);
         else if (current_idx == req_idx) skill.req = atoi(word);
         else if (current_idx == desc_idx + to_underlying(lang_idx)) memcpy(skill.desc, word, 32);
         ++current_idx;
      }
      if (current_line > 0) skill_levels.push_back(skill);
      ++current_line;
   });

   Io::read_csv_data("../Assets/Data/wound_levels.csv", [lang_idx](const char *line) {
      static int current_line{ 0 };
      stringstream ss{ line };
      char word[1024]{ "" };
      int current_idx{ 0 };
      Wound_level wound{};
      while (ss.getline(word, 1024, ',')) {
         if (current_line == 0) continue; // Header line.
         if (current_idx == 0) wound.level = atoi(word);
         if (current_idx == 1) wound.required_damage = atoi(word);
         else if (current_idx == 2 + to_underlying(lang_idx)) memcpy(wound.desc, word, 32);
         ++current_idx;
      }
      if (current_line > 0) wound_levels.push_back(wound);
      ++current_line;
   });

   Io::read_csv_data("../Assets/Data/health_levels.csv", [lang_idx](const char *line) {
      static int current_line{ 0 };
      stringstream ss{ line };
      char word[1024]{ "" };
      int current_idx{ 0 };
      Health_level health{};
      while (ss.getline(word, 1024, ',')) {
         if (current_line == 0) continue; // Header line.
         if (current_idx == 0) health.level = atoi(word);
         else if (current_idx == 1 + to_underlying(lang_idx)) memcpy(health.desc, word, 32);
         ++current_idx;
      }
      if (current_line > 0) health_levels.push_back(health);
      ++current_line;
   });

   Io::read_csv_data("../Assets/Data/fight_logs.csv", [lang_idx](const char *line) {
      constexpr int level_idx{ 0 };
      constexpr int language_idx{ 1 };
      static int current_line{ 0 };
      stringstream ss{ line };
      char word[1024]{ "" };
      int current_idx{ 0 };
      int current_level{ 0 };
      while (ss.getline(word, 1024, ',')) {
         if (current_line == 0) continue; // Header line.
         if (current_idx == level_idx) current_level = atoi(word);
         else if (current_idx == language_idx + to_underlying(lang_idx)) {
            char *str{ new char[strlen(word)] };
            memcpy(str, word, strlen(word));
            str[strlen(word)] = '\0';
            fight_logs[current_level].push_back(str);
         }
         ++current_idx;
      }
      ++current_line;
   });
}

// Status.
//---------------------------------------------------------------------------------------------------------------------
// Skill levels are stored as xp, to get actual level divide by xp_by_level.
constexpr int xp_by_level{ 1000 };

// Attributes: fixed by character type.
enum class Attribute_level : int { small = 1, medium = 2, big = 3, heroic = 3, boss = 4 };
enum class Attribute { toughness, reaction, perception };

enum class Skill {
   // Combat skills.
   combat_skills_start = 100,
   brawler, fighter, swordsman, spearman, assassin, ranger, thrower,
   combat_skills_end = 199,

   // Speech skills.
   speech_skills_start = 200,
   persuasion, intimidation, inspiration,
   speech_skills_end = 299,

   // Magic skills.
   magic_skills_start = 300,
   //white_magic, black_magic, fire_magic, water_magic, air_magic, earth_magic,
   magic_skills_end = 399,

   // Sneak skills.
   sneak_skills_start = 400,
   sneak_skills_end = 499,

   // Misc skills.
   misc_skills_start = 500,
   //mounting,
   misc_skills_end = 599,
};

Skill_level xp2level(int xp) { return static_cast<Skill_level>(xp / xp_by_level); }

int dF() {// Roll a d6 and convert to dF.
   int d6{ rand() % 6 + 1 };
   if (d6 <= 2) return -1;
   if (d6 <= 4) return 0;
   return 1;
}

int skill_roll(int level) {
   array<int, 4> rolls{ dF(), dF(), dF(), dF() };
   int sum{ reduce(rolls.begin(), rolls.end()) };
   return level + sum; // TODO: clamp to highest/lowest Skill_level.
}

// Traits: gifts and flaws, you either have it or you don't.
enum class Trait {
   // Gifts.
   avatar, // Avatars respawn after their attribute_level in days.
   night_vision,
   silver_tongue,
   gorgeous,
   ambidextrous,
   courageous,

   // Flaws.
   coward,
};

// TODO: fatigue table.

// Equipment.
//---------------------------------------------------------------------------------------------------------------------
struct Equipment {
   const char *name;
   int attack_bonus, defense_bonus;
};

struct Weapon : Equipment {
   Skill skill;
};

// Characters.
//---------------------------------------------------------------------------------------------------------------------
struct Character_sheet {
   const char *name;
   int alignment;
   int prophecy_points;
   unordered_map<Attribute, Attribute_level> attributes;
   unordered_map<Skill, int> skills;
   //unordered_map<Rank, Rank_level> ranks;
   //unordered_set<Trait> traits;
};

struct Character {
   enum Kind { pc, werewolf };
   Character(Kind kind) : kind_{ kind }, wounds_(wound_levels.size()) {
      switch (kind) {
      case Kind::pc: {
         sheet_.name = "Hero";
         sheet_.alignment = 0;
         sheet_.prophecy_points = 0;
         sheet_.attributes[Attribute::toughness] = Attribute_level::heroic;
         sheet_.skills[Skill::swordsman] = 7;
         weapon_.skill = Skill::swordsman;
      } break;
      case Kind::werewolf: {
         sheet_.name = "Werewolf";
         sheet_.alignment = -100;
         sheet_.prophecy_points = 0;
         sheet_.attributes[Attribute::toughness] = Attribute_level::medium;
         sheet_.skills[Skill::brawler] = 6;
         weapon_.skill = Skill::brawler;
      } break;
      }
   }

   bool is_alive() {
      Health_level wl{ health_level(wounds_, to_underlying(sheet_.attributes[Attribute::toughness])) };
      return wl.level != health_levels.back().level;
   }

   const char *get_name() const { return sheet_.name; }
   Health_level get_health() { return health_level(wounds_, to_underlying(sheet_.attributes[Attribute::toughness])); }

   // TODO:
   int get_skill_bonus(Skill skill) { return sheet_.skills[skill]; }

   Skill get_weapon_skill() const { return weapon_.skill; }

   void apply_damage(Wound_level wl) {
      if (wl.level >= 0)
         ++wounds_[wl.level];
   }

private:
   Kind kind_;
   Character_sheet sheet_;
   Wounds_table wounds_;
   Weapon weapon_;
   // TODO: inventory.
   // TODO: equipment.
};

// Combat.
//---------------------------------------------------------------------------------------------------------------------
struct Fight_strategy {
   Character &source, &target;
   // TODO: activation condition.
   // TODO: ability.
};

// Fight log format string special characters:
// %w = winner / %l = loser.
// In the following lines, w can be replaced by l to mean the same property about the loser.
// %wn = winner name;
// %ws = winner skill;
// %wpe = winner personal pronoun (he/she/it);
// %wpo = winner possessive pronoun (his/her/its);
// %ww = winner weapon name (e.g. sword of flamming justice);
// %wa = winner armour name (e.g. coat of plates);
const char *extract_character_property(const Character &c, const char *prop)
{
   if (!strcmp(prop, "n")) return c.get_name();
   if (!strcmp(prop, "s")) return "skill";
   if (!strcmp(prop, "pe")) return "he";
   if (!strcmp(prop, "po")) return "his";
   if (!strcmp(prop, "w")) return "weapon";
   if (!strcmp(prop, "a")) return "armour";
   abort();
}

char *get_fight_log(int level, const Character &winner, const Character &loser)
{
   const char *fmt{ fight_logs[level][0] };
   char *log{ new char[256] };

   for (size_t i = 0, j = 0; i < strlen(fmt); ++i) {
      if (fmt[i] == '%') {
         const Character *obj{ nullptr };
         switch (fmt[++i]) {
         case 'w': obj = &winner; break;
         case 'l': obj = &loser; break;
         default: abort();
         } ++i;
         char prop_str[3]{ "" };
         while (fmt[i] && isalpha(fmt[i])) prop_str[strlen(prop_str)] = fmt[i++];
         --i;
         //const char *sub{ fprop(obj) };
         //const char *sub{ properties[prop_str](obj) };
         const char *sub{ extract_character_property(*obj, prop_str) };
         memcpy(log + j, sub, strlen(sub));
         j += strlen(sub);
      } else {
         log[j++] = fmt[i];
      }
      log[j] = '\0';
   }
   return log;
}

}
