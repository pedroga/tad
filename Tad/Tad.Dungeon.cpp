module;

#include "Nag.hpp"

export module Tad.Dungeon;

import Tad.Sheet;
import Nag;
import std;

using namespace Nag;

export namespace Tad {

struct Dungeon {
   enum Direction { north = 0, south, west, east, up, down };
   std::array<std::string_view, 6> direction_str{ "north", "south", "west", "east", "up", "down" };

   struct Treasure {
      std::string description;
      Inventory inventory;
   };

   static Dungeon load(Int level) {
      Dungeon dungeon;
      dungeon.load_tutorial_level();
      return dungeon;
   }

   Bool is_loaded() const;
   std::string move_player(Direction direction); // Returns response. TODO: [[nodiscard]]

   std::string_view get_description() const; // Returns current room description.
   std::vector<Sheet> &get_monsters(); // Returns monsters in current room.
   Treasure get_treasure() {
      return rooms_[current_room_].treasure;
   }

private:
   struct Room {
      Room() : neighboors{ -1, -1, -1, -1, -1, -1 }, description{}, is_exit{ false }, monsters{}, treasure{} {}

      Int neighboors[6]; // Neighboring rooms by direction (-1 if none).
      std::string description;
      Bool is_exit;
      std::vector<Sheet> monsters;
      Treasure treasure;
   };

   // TODO: procedurally generate levels according to specifications.
   // Fixed levels.
   Dungeon() : current_room_{ -1 }, entrance_room_{ -1 } {}

   void load_tutorial_level();


   Int current_room_;  // The room the player is currently in.
   Int entrance_room_; // The room in which the player starts.
   std::vector<Room> rooms_;
};
}

namespace Tad {

Bool Dungeon::is_loaded() const
{
   return !rooms_.empty();
}

std::string Dungeon::move_player(Direction direction)
{
   Int next_room = rooms_[current_room_].neighboors[direction];
   if (next_room < 0) return "Invalid direction!";
   current_room_ = next_room;
   return std::format("You moved {}", direction_str[direction]);
}

std::string_view Dungeon::get_description() const
{
   return rooms_[current_room_].description;
}

std::vector<Sheet> &Dungeon::get_monsters()
{
   auto &monsters{ rooms_[current_room_].monsters };
   std::erase_if(monsters, [](auto monster) { return monster.hp <= 0; });
   return monsters;
}

void Dungeon::load_tutorial_level() //+--------------------------------------------------------------------------------
{
   rooms_.resize(18);
   current_room_ = entrance_room_ = 5;

   // TODO: distribute monsters and treasure.
   rooms_[10].monsters.push_back({ skeleton_sheet });
   rooms_[0].monsters.push_back({ skeleton_sheet });
   rooms_[1].treasure = Treasure{ "a gold pouch", 50 };

   // Exits.
   rooms_[5].is_exit = rooms_[9].is_exit = true;

   // Room 0: Hallway.
   rooms_[0].description = "You are in a hallway. "
      "There is a door to the south."
      "Through windows to the north you can see a secret herb garden.";
   rooms_[0].neighboors[west] = 5;
   rooms_[0].neighboors[south] = 1;

   // Room 1: Audience Chamber.
   rooms_[1].description = "This is the audience chamber. There is a window"
      " to the west, by looking through it you can see the entrance to the "
      "castle. Doors leave this room to the north, east and south.";
   rooms_[1].neighboors[north] = 0;
   rooms_[1].neighboors[south] = 2;
   rooms_[1].neighboors[east] = 2;

   // Room 2: Great Hall.
   rooms_[2].description = "You are in the great hall, an L-shaped room. "
      "There are doors to the east and to the north. "
      "In the alcove, there is a door to the west.";
   rooms_[2].neighboors[north] = 1;
   rooms_[2].neighboors[west] = 1;
   rooms_[2].neighboors[east] = 4;

   // Room 3: Private Meeting.
   rooms_[3].description = "This is the monarch's private meeting room. "
      "There is a single exit to the south.";
   rooms_[3].neighboors[south] = 4;

   // ROOM 4. Inner Hallway.
   rooms_[4].description = "This inner hallway contains a door to the north, "
      "and one to the west, and a circular stairwell passes through the room. "
      "You can see an ornamental lake through the windows to the south.";
   rooms_[4].neighboors[north] = 3;
   rooms_[4].neighboors[west] = 2;
   rooms_[4].neighboors[down] = 11;
   rooms_[4].neighboors[up] = 13;

   // Room 5: Entrance.
   rooms_[5].description = "You are at the entrance to a forbidding-looking "
      "stone castle. You are facing east.";
   rooms_[5].neighboors[east] = 0;

   // Room 6: Kitchen.
   rooms_[6].description = "This is the castle's kitchen. "
      "Through windows in the north wall you can see a secret herb garden. "
      "A door leaves the kitchen to the south. ";
   rooms_[6].neighboors[south] = 7;

   // Room 7: Store Room.
   rooms_[7].description = "You are in the store room, amidst spices, "
      "vegetables, and vast sacks of flour and other provisions. "
      "There is a door to the north and one to the south.";
   rooms_[7].neighboors[north] = 6;
   rooms_[7].neighboors[south] = 8;

   // Room 8: Rear Vestibule.
   rooms_[8].description = "You are in the rear vestibule. "
      "There are windows to the south from which you can see the ornamental lake. "
      "There is an exit to the east, and one to the north. "
      "A lift going up can be used too.";
   rooms_[8].neighboors[north] = 7;
   rooms_[8].neighboors[east] = 9;
   rooms_[8].neighboors[up] = 17;

   // Room 9: Exit.
   rooms_[9].description = "You have made it out of the castle. "
      "You can still go back west. Do you wish to leave?";
   rooms_[9].neighboors[west] = 8;

   // ROOM 10. Dungeon.
   rooms_[10].description = "You are in the dank, dark dungeon. "
      "There is a single exit, a small hole in the wall towards the west.";
   rooms_[10].neighboors[west] = 11;

   // ROOM 11. Guard Room.
   rooms_[11].description, "You are in the prison guardroom, in "
      "the basement of the castle. The stairwell ends in this room. "
      "There is one othe exit, a small hole in the east wall.";
   rooms_[11].neighboors[east] = 10;
   rooms_[11].neighboors[up] = 4;

   // ROOM 12. Master Bedroom.
   rooms_[12].description = "You are in the master bedroom on the upper "
      "level of the castle. Looking down from the window to the west you "
      "can see the entrance to the castle, while the secret herb garden is "
      "visible below the north window. There are doors to the east and "
      "to the south.";
   rooms_[12].neighboors[south] = 13;
   rooms_[12].neighboors[east] = 15;

   // ROOM 13. Upper Hallway.
   rooms_[13].description = "This is the L-shaped upper hallway. "
      "To the north is a door, and there is a stairwell in the hall as well. "
      "You can see the lake through the south windows. ";
   rooms_[13].neighboors[north] = 12;
   rooms_[13].neighboors[down] = 4;

   // ROOM 14. Treasury.
   rooms_[14].description = "This room was used as the castle treasury in "
      "bygone years... there are no windows, just exits to the north and east.";
   rooms_[14].neighboors[north] = 15;
   rooms_[14].neighboors[east] = 17;
   rooms_[14].treasure = Treasure{ "a small pile of gold coins", 100 };

   // ROOM 15. Chambermaid's Bedroom.
   rooms_[15].description = "Ooooh... You are in the chambermaid's bedroom. "
      "There is an exit to the west and a door to the south, and to the north.";
   rooms_[15].neighboors[north] = 16;
   rooms_[15].neighboors[south] = 14;
   rooms_[15].neighboors[west] = 12;

   // ROOM 16. Dressing chamber.
   rooms_[16].description = "This tiny room on the upper level is the dressing "
      "chamber. There is a window to the north, with a view of the secret garden "
      "down below. A door leaves to the south. ";
   rooms_[16].neighboors[south] = 15;

   // ROOM 17. Lift room.
   rooms_[17].description = "This is the small room outside the castle in which "
      "the lift going down can be entered. Another door leads to the west. "
      "You can see the lake through the southern windows. ";
   rooms_[17].neighboors[west] = 14;
   rooms_[17].neighboors[down] = 8;
}

}
