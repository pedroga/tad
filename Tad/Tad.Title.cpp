module;

#include "Nag.hpp"

export module Tad.Title;

import Tad.Assets;
import Tad.Game;
import Nag;

using namespace Nag;

export namespace Tad {

struct Title_mode : App_mode
{//+-----------------------------------------------------------------------------------------------------------------------------

   Title_mode()
      : window_{ "TAD" }
      , font_{ window_.load_font(Assets::default_font, 100) }
      , gradient_{ window_.create_image(1, 256) }, bg_{ window_.load_image(Assets::title_bg) }
      , layout_{ window_ }
   {//+--------------------------------------------------------------------------------------------------------------------------
      // Ui.

      window_.set_size(1600, 900);
      window_.set_position(Window::centered, Window::centered);
      gradient_.paint_gradient({ 255, 0, 255, 255 }, color_white);
      layout_.add_frame<Pane_frame>(100, Pane_orientation::horizontal, .42f);
      layout_.add_frame<Pane_frame>(150, Pane_orientation::horizontal, .3f);
      layout_.add_frame<Pane_frame>(125, Pane_orientation::vertical, .3f);
      menu_ = &layout_.add_frame<Menu_frame>(130, font_, Menu_frame::Orientation::vertical, 0);
      menu_->add_option("Play", [this] {set_game_mode(); });
      menu_->add_option("Conf");
      menu_->add_option("Exit", [] { quit(); });

      // Input.

      auto handle_confirmation = [this](Bool down) { if (down) { menu_->do_action(); } };
      get_input().set_blocking(true);
      btn_handlers_[Gamepad_btn::down] = [this](Bool down) { if (down) menu_->next_option(); };
      btn_handlers_[Gamepad_btn::up] = [this](Bool down) { if (down) menu_->prev_option(); };
      btn_handlers_[Gamepad_btn::a] = handle_confirmation;
      fun_handlers_[Function_key::down] = [this](Bool down) { if (down) menu_->next_option(); };
      fun_handlers_[Function_key::up] = [this](Bool down) { if (down) menu_->prev_option(); };
      fun_handlers_[Function_key::enter] = handle_confirmation;
   }

   void update() override {}

   void redraw() override
   {//+--------------------------------------------------------------------------------------------------------------------------
      Image target{ window_.get_target() };
      target.copy(gradient_);
      target.copy(bg_);
      layout_.draw();
      window_.refresh();
   }

private:

   void set_game_mode()
   {//*--------------------------------------------------------------------------------------------------------------------------
      app_mode_stack__.push(new Game_mode{ window_ });
   }

   //* Member variables. ========================================================================================================

   Window window_;
   Font font_;
   Image bg_, gradient_;
   Frame_layout layout_;
   Menu_frame *menu_;
};
}
