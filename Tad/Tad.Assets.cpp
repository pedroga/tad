export module Tad.Assets;

#define ASS_BASE "../Assets"
export namespace Tad {
export namespace Assets {
using Ass_path = const char *;
constexpr Ass_path default_font{ ASS_BASE"/Fonts/default.ttf" };
constexpr Ass_path title_bg{ ASS_BASE"/Art/tad-thumb-trans.png" };
constexpr Ass_path title_music{ ASS_BASE"/Musics/wander-143535.mp3" };
constexpr Ass_path sound_change_option{ ASS_BASE"/Sounds/typewriter-scrolling-44786.wav" };
constexpr Ass_path sound_accept_option{ ASS_BASE"/Sounds/typewriter-bell-100087.wav" };

}
}
