module;

#include "Nag.hpp"

export module Tad.Game;

import Tad.Assets;
import Tad.Dungeon;
import Nag;
import std;

using namespace Nag;

export namespace Tad {

struct Game_mode : App_mode
{//+-----------------------------------------------------------------------------------------------------------------------------

   using Log_frame = Log_frame<20>;

   enum class Game_state { fight, travel, dungeon, dialog, guild, game_over };

   //+ Player action read from input. ===========================================================================================

   using Move_action = Dungeon::Direction;
   enum class Fight_action { fight, flight };
   enum class Menu_action { left, right, accept };
   enum class Interaction { pick_item };
   using Player_action = std::variant<std::monostate, Move_action, Fight_action, Menu_action, Interaction>;

   Game_mode(Window &window) 
      // Ui.
      : window_{ window }, font_{ window_.load_font(Assets::default_font, 50) }
      , layout_{ window_ }
      // Game state.
      , quit_game_{ false }, game_state_{ Game_state::dungeon }
      // Game data.
      , hero_sheet_{ "Thelsolo", "The Hero", 50, 5 }, dungeon_{ new Dungeon{Dungeon::load(0)} }
   {//+--------------------------------------------------------------------------------------------------------------------------
      std::srand(std::time(0));

      // Ui.
      layout_.add_frame<Pane_frame>(128, Pane_orientation::vertical, 0.9f);
      layout_.add_frame<Pane_frame>(64, Pane_orientation::horizontal, 0.75f);
      log_frame_ = &(layout_.add_frame<Log_frame>(32) << font_);
      context_frame_ = &layout_.add_frame<Context_frame>(196);
      menu_frame_ = &(layout_.add_frame<Menu_frame>(196, font_)
         .add_option("North", [this] { player_action_ = Move_action{ Dungeon::north }; })
         .add_option("South", [this] { player_action_ = Move_action{ Dungeon::south }; })
         .add_option("East", [this] { player_action_ = Move_action{ Dungeon::east }; })
         .add_option("West", [this] { player_action_ = Move_action{ Dungeon::west }; })
         .add_option("Up", [this] { player_action_ = Move_action{ Dungeon::up }; })
         .add_option("Down", [this] { player_action_ = Move_action{ Dungeon::down }; })
         .add_option("Fight", [this] { player_action_ = Fight_action::fight; })
         .add_option("Flight", [this] { player_action_ = Fight_action::flight; })
         .set_colors(Color{ 0xFF, 0x00, 0x00 }, Color{ 0x52, 0x52, 0x52 })
      );
      fight_menu_frame_ = &(layout_.add_frame<Menu_frame>(196, font_)
         .add_option("Fight", [this] { player_action_ = Fight_action::fight; })
         .add_option("Flight", [this] { player_action_ = Fight_action::flight; })
         .set_colors(Color{ 0xFF, 0x00, 0x00 }, Color{ 0x52, 0x52, 0x52 })
      );

      // Gamepad Input.
      btn_handlers_[Gamepad_btn::back] = [this](Bool down) { quit_game_ = true; };
      btn_handlers_[Gamepad_btn::a] = [this](Bool down) { if (down) { player_action_ = Menu_action::accept; } };
      btn_handlers_[Gamepad_btn::left] = [this](Bool down) { if (down) { player_action_ = Menu_action::left; } };
      btn_handlers_[Gamepad_btn::right] = [this](Bool down) { if (down) { player_action_ = Menu_action::right; } };
      btn_handlers_[Gamepad_btn::up] = [this](Bool down) { if (down) { *log_frame_ << Log_scroll{ 1 }; } };
      btn_handlers_[Gamepad_btn::down] = [this](Bool down) { if (down) { *log_frame_ << Log_scroll{ -1 }; } };

      // Keyboard Input.
      fun_handlers_[Function_key::escape] = [this](Bool down) {quit_game_ = true; };
      fun_handlers_[Function_key::enter] = [this](Bool down) { if (down) { player_action_ = Menu_action::accept; } };
      fun_handlers_[Function_key::left] = [this](Bool down) { if (down) { player_action_ = Menu_action::left; } };
      fun_handlers_[Function_key::right] = [this](Bool down) { if (down) { player_action_ = Menu_action::right; } };
      fun_handlers_[Function_key::pageup] = [this](Bool down) { if (down) { *log_frame_ << Log_scroll{ 1 }; } };
      fun_handlers_[Function_key::pagedown] = [this](Bool down) { if (down) { *log_frame_ << Log_scroll{ -1 }; } };
      fun_handlers_[Function_key::space] = [this](Bool down) { if (down) { *log_frame_ << Log_option::endl; } };
      key_handlers_['w'] = [this](Bool down) { if (down) { player_action_ = Move_action{ Dungeon::west }; } };
      key_handlers_['n'] = [this](Bool down) { if (down) { player_action_ = Move_action{ Dungeon::north }; } };
      key_handlers_['u'] = [this](Bool down) { if (down) { player_action_ = Move_action{ Dungeon::up }; } };
      key_handlers_['d'] = [this](Bool down) { if (down) { player_action_ = Move_action{ Dungeon::down }; } };
      key_handlers_['e'] = [this](Bool down) { if (down) { player_action_ = Move_action{ Dungeon::east }; } };
      key_handlers_['s'] = [this](Bool down) { if (down) { player_action_ = Move_action{ Dungeon::south }; } };

      key_handlers_['f'] = [this](Bool down) { if (down) { player_action_ = Fight_action::fight; } };
      key_handlers_['l'] = [this](Bool down) { if (down) { player_action_ = Fight_action::flight; } };

      // Describe entrance.
      log(dungeon_->get_description());
   }

   void update() override
   {//+--------------------------------------------------------------------------------------------------------------------------
      if (quit_game_) { app_mode_stack__.pop(); return; }
      switch (game_state_) {
      case Game_state::dungeon: {
         dungeon_mode_update();
      } break;
      case Game_state::fight: {
         fight_mode_update();
      } break;
      } player_action_ = {};
   }

   void redraw() override
   {//+--------------------------------------------------------------------------------------------------------------------------
      window_.get_target().clear(Color{ 0x0F, 0x0F, 0x00 });
      layout_.draw();
      window_.refresh();
   }

private:

   void dungeon_mode_update()
   {//*--------------------------------------------------------------------------------------------------------------------------
check_input:
      if (auto *action = std::get_if<Move_action>(&player_action_)) {
         dungeon_move(*action);
      } else if (auto *action = std::get_if<Fight_action>(&player_action_)) {
         if (*action == Fight_action::fight) {
            if (dungeon_->get_monsters().empty()) log("There is no one to fight here");
            else state_transition(Game_state::fight);
         }
      } else if (auto *action = std::get_if<Menu_action>(&player_action_)) {
         switch (*action) {
         case Menu_action::left: menu_frame_->prev_option(); break;
         case Menu_action::right: menu_frame_->next_option(); break;
         case Menu_action::accept: menu_frame_->do_action(); goto check_input;
         }
      }
   }

   void fight_mode_update()
   {//*--------------------------------------------------------------------------------------------------------------------------
check_input:
      if (auto *action = std::get_if<Fight_action>(&player_action_)) {
         switch (*action) {
         case Fight_action::fight:
            fight_round();
            break;
         case Fight_action::flight:
            state_transition(Game_state::dungeon);
            break;
         }
      } else if (auto *action = std::get_if<Menu_action>(&player_action_)) {
         switch (*action) {
         case Menu_action::left: fight_menu_frame_->prev_option(); break;
         case Menu_action::right: fight_menu_frame_->next_option(); break;
         case Menu_action::accept: fight_menu_frame_->do_action(); goto check_input;;
         }
      }
   }

   void state_transition(Game_state state)
   {//*--------------------------------------------------------------------------------------------------------------------------
      switch (state) {
      case Game_state::fight:
         context_frame_->set_context(1);
         break;
      case Game_state::dungeon:
         context_frame_->set_context(0);
         break;
      }
      game_state_ = state;
   }

   void dungeon_move(Dungeon::Direction d)
   {//*--------------------------------------------------------------------------------------------------------------------------
      if (game_state_ != Game_state::dungeon)
         throw Error{ std::format("game_state_ is [{}] / Expected dungeon", std::to_underlying(game_state_)) };
      // Todo: check if enemy has not engaged.
      log(dungeon_->move_player(d).c_str());
      log(dungeon_->get_description());
      if (!dungeon_->get_treasure().description.empty()) {
         Int treasure_money{ dungeon_->get_treasure().inventory.money };
         log(std::format("You see {} ({})", dungeon_->get_treasure().description, treasure_money.to_underlying()));
      }
      for (auto &monster : dungeon_->get_monsters()) {
         log(monster.description);
      }
   }

   void log(std::string_view msg)
   {//*--------------------------------------------------------------------------------------------------------------------------
      static Unt color_alt{ 0 };
      constexpr Color text_color_1{ Color{ 0x36, 0x6B, 0x36 } };
      constexpr Color text_color_2{ Color{0x6b, 0x36, 0x6b} };
      *log_frame_
         << ((color_alt++ & 1) ? text_color_1 : text_color_2) // Set alternating color.
         << msg << Log_option::endl;
   }

   void fight_round()
   {//*--------------------------------------------------------------------------------------------------------------------------
      Int hero_roll{ skill_roll(hero_sheet_.lvl) };
      log(std::format("Hero rolls {}", hero_roll.to_underlying()));
      for (auto &monster_sheet : dungeon_->get_monsters()) {// Roll for each monster.
         if (monster_sheet.hp <= 0) continue;
         Int monster_roll{ skill_roll(monster_sheet.lvl) };
         log(std::format("Monster rolls {}", monster_roll.to_underlying()));
         if (hero_roll == monster_roll) {// Tie.
            log(std::format("{} and {} are unable to hit each other", hero_sheet_.name, monster_sheet.name));
            continue;
         }
         auto [winner, loser] { hero_roll > monster_roll ?
            std::pair{ &hero_sheet_, &monster_sheet } : std::pair{ &monster_sheet, &hero_sheet_ } };
         Int damage{ std::abs(hero_roll - monster_roll) }; // TODO: modifiers.
         loser->hp -= damage;
         if (loser->hp <= 0) {
            log(std::format("{} slayed {}", winner->name, loser->name));
            if (loser == &hero_sheet_) {
               state_transition(Game_state::game_over);
               break;
            }
         } else {
            log(std::format("{} hit {} causing {} points of damage", winner->name, loser->name, damage.to_underlying()));
            log(std::format("Now {} has {} hit points", loser->name, loser->hp.to_underlying()));
         }
      }
      if (dungeon_->get_monsters().empty()) {// All monsters were slayed.
         log("The fight ended");
         state_transition(Game_state::dungeon);
      }
   }

   Int skill_roll(Int level)
   {//*--------------------------------------------------------------------------------------------------------------------------
      Int result{ 0 };
      for (Int i{ 0 }; i < level; ++i)// Roll 1d6 for each level.
         if (1 + std::rand() % 6 > 3) ++result; // 4, 5, 6 counts as a positive result.
      return result;
   }

   //* Member variables =========================================================================================================

   //* Ui.

   Window &window_;
   Font font_;
   Frame_layout layout_;
   Log_frame *log_frame_;
   Context_frame *context_frame_;
   Menu_frame *menu_frame_, *fight_menu_frame_;

   //* Game loop.

   Bool quit_game_;
   Game_state game_state_;
   Player_action player_action_;

   //* Game data.

   Sheet hero_sheet_;
   std::unique_ptr<Dungeon> dungeon_;

};

}
