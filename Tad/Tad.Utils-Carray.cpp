export module Tad.Utils:Carray;

import std;
import :Common;

export namespace Utils {
template <typename T, int C> struct Carray {// Circular fixed-size array.
   static constexpr int cap = C + 1; // One extra element for the end()/rend() sentinel.
   struct Iterator {
      Iterator(Carray &c, int idx) : container_{ c }, idx_{ idx } {}
      T &operator*() const { return container_[idx_]; }
      T *operator->() { return &container_[idx_]; }
      Iterator &operator++() { ++idx_; return *this; }
      Iterator &operator--() { --idx_; return *this; }
      Iterator operator+(int offset) { auto copy{ *this }; copy.idx_ += offset; return copy; }
      friend bool operator==(const Iterator &a, const Iterator &b) {
         return (&a.container_ == &b.container_) && (a.idx_ == b.idx_);
      }
      friend bool operator!=(const Iterator &a, const Iterator &b) {
         return !(operator==(a, b));
      }

   private:
      Carray<T, C> &container_;
      int idx_;
   };

   struct Reverse_iterator : Iterator {
      Reverse_iterator(Carray<T, C> &c, int idx) : Iterator{ c, idx } {}
      Reverse_iterator &operator++() { Iterator::operator--();  return *this; }
      Reverse_iterator operator+(int offset) {
         auto copy{ *this };
         for (int i{ offset }; i > 0; --i) copy.operator--();
         for (int i{ offset }; i < 0; ++i) copy.operator++();
         return copy;
      }
   };

   Carray() : first_{ 0 }, len_{ 0 }, data_{} {}
   Iterator begin() { return Iterator(*this, 0); }
   Iterator end() { return Iterator(*this, len_); }
   Reverse_iterator rbegin() { return { *this, len_ - 1 }; }
   Reverse_iterator rend() { return { *this, -1 }; }

   void push_back(T val) {
      if (full()) return;
      data_[(first_ + len_++) % cap] = val;
   }

   void push_front(T val) {
      if (full()) return;
      if (--first_ < 0) first_ = cap - 1;
      data_[first_] = val;
      if (len_ < cap) ++len_;
   }

   void pop_back() {
      if (--len_ < 0) len_ = 0;
   }

   void pop_front() {
      if (len_ == 0) return;
      first_ = (first_ + 1) % cap;
      --len_;
   }

   T &back() {
      TAD_assert(!empty(), "Accessing element from empty container");
      return data_[(first_ + len_ - 1) % cap];
   }

   T &front() {
      TAD_assert(!empty(), "Accessing element from empty container");
      return data_[first_];
   }

   T &operator[](int idx) {
      if (idx < 0 || idx > len_) throw std::out_of_range{ "Index out of range" };
      return data_[(first_ + idx) % cap];
   }

   int size() { return len_; }
   bool full() { return len_ == C; }
   bool empty() { return !len_; }

private:
   int first_, len_;
   T data_[cap];
};
}
