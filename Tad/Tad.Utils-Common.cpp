export module Tad.Utils:Common;

import std;
using namespace std;

export namespace Utils {
void TAD_fatal(const char *msg) { std::cerr << msg << '\n'; abort(); }
void TAD_assert(bool cond, const char *msg) { if (!(cond)) { TAD_fatal(msg); } }

template<typename Val_t, class Tag>
struct Distinct {
   Distinct() : val_() {}
   Distinct(Val_t val) : val_(val) {}
   operator Val_t () const { return val_; }
   Distinct &operator=(Distinct const &v) {
      val_ = v.val_;
      return *this;
   }
   bool operator<(Distinct<Val_t, Tag> o) { return val_ < o.val_; }
   bool operator!=(Distinct<Val_t, Tag> o) { return val_ != o.val_; }

   Val_t get() { return val_; }
private:
   Val_t val_;
};

template<typename T>
T round_clamp(T val, T lo, T hi)
{
   if (val < lo) return hi;
   if (val > hi) return lo;
   return val;
}

bool is_inside(int x, int y, int rx, int ry, int rw, int rh)
{
   return (rx <= x && x <= rx + rw) && (ry <= y && y <= ry + rh);
}

}


/*
template<typename T, int64_t max, bool round>
struct Evar {
   Evar(T t) : var_{ t } { TAD_assert(static_cast<uint64_t>(t) < max, "Creating Evar with value above max"); }

   bool operator==(T t) {
      return var_ == t;
   }

   template<typename S>
   bool operator==(S s) {
      T t{ static_cast<T>(s) };
      return var_ == t;
   }

   T &operator=(underlying_type<T> t) { var_ = static_cast<T>(t); }
   operator T () const { return var_; }

   void operator++() {
      var_ = round ?
         static_cast<T>(round_clamp(static_cast<int64_t>(var_) + 1, static_cast<int64_t>(0), static_cast<int64_t>(max) - 1)) :
         static_cast<T>(clamp(static_cast<int64_t>(var_) + 1, static_cast<int64_t>(0), static_cast<int64_t>(max) - 1));
   }

   void operator--() {
      var_ = round ?
         static_cast<T>(round_clamp(static_cast<int64_t>(var_) - 1, static_cast<int64_t>(0), static_cast<int64_t>(max) - 1)) :
         static_cast<T>(clamp(static_cast<int64_t>(var_) - 1, static_cast<int64_t>(0), static_cast<int64_t>(max) - 1));
   }


private:
   T var_;
};
*/
