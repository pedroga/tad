module;

#include "Nag.hpp"

export module Tad.Sheet;

import std;
import Nag;

using namespace Nag;

export namespace Tad {
struct Sheet {
   Sheet(std::string_view name, std::string_view desc, Int hp, Int lvl)
      : name{ name }, description{ desc }, hp{ hp }, lvl{ lvl } {
   }

   std::string_view name, description;
   Int hp, lvl;
};

struct Inventory {
   // TODO: items.
   Int money;
};

struct Character {
   Int id;
   Sheet sheet;
   Inventory inventory;
};

Sheet skeleton_sheet{"Skeleton", "You see an skeleton.", 5, 2 };

}
